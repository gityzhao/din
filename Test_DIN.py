import os
import math
import scipy
import scipy.misc
import mxnet as mx
import numpy as np
import time
from collections import namedtuple
from mxnet import gpu
from mxnet import image
from mxnet import gluon
import cv2
from mxnet import ndarray as nd
#from Utils_mx import modcrop, addMean,moveMean,compute_psnr,compute_ssim,imread,interlace_moveMean,interlace_moveMean_PS


#-----------------------Utils---------------------------------
def moveMean(data):
	data = data.astype(np.float32)
	return data / 255.
	#MAX_PIXEL_VAL=128
	#return (data-MAX_PIXEL_VAL)/MAX_PIXEL_VAL
	
def addMean(data):
	data = data.astype(np.float32)
	return data * 255.
	#MAX_PIXEL_VAL=128
	#return data*MAX_PIXEL_VAL + MAX_PIXEL_VAL

def modcrop(img_in, scale):
    """img_in: Numpy, HWC or HW"""
    img = np.copy(img_in)
    if img.ndim == 2:
        H, W = img.shape
        H_r, W_r = H % scale, W % scale
        img = img[:H - H_r, :W - W_r]
    elif img.ndim == 3:
        H, W, C = img.shape
        H_r, W_r = H % scale, W % scale
        img = img[:H - H_r, :W - W_r, :]
    else:
        raise ValueError('Wrong img ndim: [{:d}].'.format(img.ndim))
    return img

def interlace_moveMean_PS(data):
	# split and using half-Height for pixel-shuffle mode
	data = data.astype(np.float32)
	up = data[:,:,::2,:]
	down = data[:,:,1::2,:]
	new_data = nd.concat(up,down,dim=1)
	return new_data / 255.
	#MAX_PIXEL_VAL=128
	#return (data-MAX_PIXEL_VAL)/MAX_PIXEL_VAL

def interlace_moveMean(data):
	data = data.astype(np.float32)
	up = data
	down = data
	up[:,:,1::2,:] = data[:,:,::2,:]
	down[:,:,::2,:] = data[:,:,1::2,:]
	new_data = nd.concat(up,down,dim=1)
	return new_data / 255.
	#MAX_PIXEL_VAL=128
	#return (data-MAX_PIXEL_VAL)/MAX_PIXEL_VAL

Batch = namedtuple('label', ['data'])
#-------------------------------------------------------------

#----------------------Parameters Here------------------------
# model information
#-------------DIN-------------
model_prefix = "model//DIN"
EpochNum=50
#EpochNum=100
#----------Light DIN----------
#model_prefix = "model//LDIN"
#EpochNum=120

zooming=1
ctx=mx.gpu(0)
LWN_model='_DIN_'
testSet='OLD'
#-------------------------------------------------------------

data_path =os.getcwd() + '//Test//'+testSet+'//IN//'
save_image_path = os.getcwd() + '//Test//'+testSet+'//results//'

data = sorted(os.listdir(data_path))

for i in range(0,len(data)):
	test_image_name = data_path+data[i]

	
	save_image_name = save_image_path+data[i]

	LRI = cv2.imread(test_image_name)
	LRI = cv2.cvtColor(LRI,cv2.COLOR_BGR2RGB)

	
	LRI = modcrop(LRI, scale=2)
	
	nh, nw, nc = LRI.shape
	

	# --------------------------------------------GLUON forward computation ----------------------------------------
		
	# loading checkpoints
	symbol_file=os.path.join(os.getcwd(),model_prefix)
	param_file=symbol_file+'-%04d.params' % (EpochNum)
	net = gluon.SymbolBlock.imports(symbol_file+"-symbol.json",["data"],param_file=param_file,ctx=ctx)
	

	if nw<2000:
		# loading input image
		tic=time.time()
		input_LR=mx.nd.array(LRI).transpose((2,0,1))
		input_LR=input_LR.reshape([1, nc, nh, nw])
		expand_LR=interlace_moveMean_PS(input_LR)
	
		# forward computation
		expand_LR=expand_LR.as_in_context(ctx)
		output=net(expand_LR)

		mod_output = output.reshape([nc, nh*zooming, nw*zooming])
		mod_output = addMean(mod_output)
		mod_output = mod_output.clip(0,255)
	
		mod_output=mod_output.asnumpy().astype(np.uint8).transpose((1,2,0))
		res_output = np.squeeze(mod_output)
		toc=time.time()-tic
	
	else:
		print("OOM for 4K+ , using split mode ... ...")
		res_output=LRI*0
		for x,y in ((0,0),(0,nw//2),(nh//2,0),(nh//2,nw//2)):
			mod_input = LRI[x:int(x + nh//2), y:int(y + nw//2),:]
			input_LR=mx.nd.array(mod_input).transpose((2,0,1))
			input_LR=input_LR.reshape([1, nc, nh//2, nw//2])
			expand_LR=interlace_moveMean(input_LR)
	
			# forward computation
			expand_LR=expand_LR.as_in_context(ctx)
			output=net(expand_LR)

			mod_output = output.reshape([nc, nh//2, nw//2])
			mod_output = addMean(mod_output)
			mod_output = mod_output.clip(0,255)
	
			mod_output=mod_output.asnumpy().astype(np.uint8).transpose((1,2,0))
			res_output_part = np.squeeze(mod_output)
			
			res_output[x:int(x + nh//2), y:int(y + nw//2),:] = res_output_part
	

	#-------------------------___SAVING IMAGES____-----------------------------------------------------	
	print("==================="+data[i]+"=========================")
	print("Time Cost:    "+str(toc))
	res_output = cv2.cvtColor(res_output,cv2.COLOR_RGB2BGR)
	cv2.imwrite(save_image_name+LWN_model+str(EpochNum)+'.png', res_output)
	

