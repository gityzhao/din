--------------------------------------
Deinterlacing (DI)
Ver 0.1 by YZhao@hfut.edu.cn

Y. Zhao, W. Jia, R. Wang, 
"Rethinking deinterlacing for early interlaced videos,"
IEEE Transactions on Circuits and Systems for Video Technology, 2021.
--------------------------------------

** Requirement: MXNET (https://mxnet.cdn.apache.org/)
$python Test_DIN.py

--------------------------------------
File Description：
|------/Test
|------------/OLD 	: 	some real-world interlaced frames
|------------/DIset : 	synthetic interlaced frames 

|------/model
|------------/DIN-0100.params 	: 	normal DIN epoch 100 model
|------------/DIN-0050.params 	: 	DIN epoch 50 model with over-smoothen
|------------/LDIN-0120.params 	: 	Lightweight version of DIN

|------/Test_DIN.py	: 	testing script for DIN
|------/DIN_demoVideo.mp4	: 	Demo video of DIN
