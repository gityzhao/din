# DIN - Deinterlacing Network for Early Interlaced Videos

#### Description
Demo videos and demo codes for "Deinterlacing Network for Early Interlaced Videos".
 (MXNET)


--------------------------------------
Deinterlacing (DI)
Ver 0.1 by YZhao@hfut.edu.cn

Y. Zhao, W. Jia, R. Wang, X. Liu, X. Gao, W. Chen, W. Gao, 
"Deinterlacing network for early interlaced videos" under review, 2020.

--------------------------------------
File Description：
|------/Test
|------------/OLD 	: 	some real-world interlaced frames
|------------/DIset : 	synthetic interlaced frames 

|------/model
|------------/DIN-0100.params 	: 	normal DIN epoch 100 model
|------------/DIN-0050.params 	: 	DIN epoch 50 model with over-smoothen
|------------/LDIN-0120.params 	: 	Lightweight version of DIN

|------/Test_DIN.py	: 	testing script for DIN
|------/DIN_demoVideo.mp4	: 	Demo video of DIN

--------------------------------------

For testing:
$python Test_DIN.py

* Requirement: MXNET (https://mxnet.cdn.apache.org/)
